// Package assert is the only assertion library you'll ever need for Go!
// It supports arbitrarily complex conditional expressions, so long as they
// evaluate to a boolean, and it can conditionally execute any code based
// on the condition.
//
// It even supports optional Else() and ElseIf() chaining!
package assert

// Chainable allows chaining of else conditions.
type Chainable interface {
	// Else will execute fn if the preceeding condition evaluates to false.
	Else(fn func())
	// ElseIf will evaluate condition if the previous condition evaluates to
	// false, and will execute fn if condition is true.
	ElseIf(condition bool, fn func()) Chainable
}

type (
	cont struct{}
	stop struct{}
)

var (
	_ Chainable = cont{}
	_ Chainable = stop{}
)

// If executes fn if condition evaluates to true.
func If(condition bool, fn func()) Chainable {
	if condition {
		fn()
		return stop{}
	}
	return cont{}
}

func (cont) Else(fn func()) {
	fn()
}

func (cont) ElseIf(condition bool, fn func()) Chainable {
	return If(condition, fn)
}

func (stop) Else(func())                   {}
func (stop) ElseIf(bool, func()) Chainable { return stop{} }
